$(document).ready(function () {
    $('#list_buku').DataTable({
        "processing": true,
            "scrollX": true,
        "ajax": {
            "url": '/api/',
        },

        "columns": [
            {
                // image thumbnail
                data: null,
                render: function (data, type, row, meta) {
                    return '<img src=' + data.volumeInfo.imageLinks.smallThumbnail + '>'

                }
            },
            {data: "volumeInfo.title", defaultContent: "<i>unknown data<i>",},{data: "volumeInfo.authors[,]",defaultContent: "<i>unknown data<i>",},
            {
                defaultContent: "<i>unknown data<i>",
                orderable: false,
                data: "accessInfo.webReaderLink",
                render: function (data, type, row, meta) {
                    return "<a style='color:blue' href='" + data + "'>Link</a>"
                }

            },
            {data: "volumeInfo.publishedDate",defaultContent: "<i>unknown data<i>",},{data: "volumeInfo.categories",defaultContent: "<i>unknown data<i>",},
            {
                orderable: false,
                defaultContent: "<i>unknown data<i>",
                data: null,
                render: function (data, type, row, meta) {
                    return "<p class='fav star-symbol' style='font-size:50px; color:blue; text-align:center;'>"+"☆"+"</p>"
                }

            }

        ],
        
    });

    var counter = document.getElementById('counter').innerHTML;
    // document.getElementById('counter').innerHTML = counter;
    
    $(document).on('click', '.fav', function () {
        if ($(this).text() == "☆") {
            counter++;
            document.getElementById('counter').innerHTML = counter;
            localStorage.setItem('counter', counter);
            $.post('/', {'jml' : counter})
            $(this).text("★");
        } else {
            counter--;
            document.getElementById('counter').innerHTML = counter;
            localStorage.setItem('counter', counter);
            $.post('/', {'jml' : counter})
            $(this).text("☆");
        }

      });

});
