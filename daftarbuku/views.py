from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpRequest, HttpResponseBadRequest,HttpResponseRedirect
import requests
import json
from .form import Searcher
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, redirect, render
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required

url_data = 'https://www.googleapis.com/books/v1/volumes?q=quilting'

# Create your views here.
@login_required(login_url='/login')
@csrf_exempt
def daftar(request):

	jml = 0
	if request.method == 'POST':
            request.session['jml'] = request.POST['jml']

	if request.session.get('jml') is not None:
            jml = request.session.get('jml')

			

	response = {'Searcher':Searcher, 'jumlah_buku': jml}
	return render(request, "buku.html",response)

def data_bukuku(request):
	json_data = requests.get(url_data)
	ret_data = {"data" : (json_data.json())["items"]}
	return HttpResponse(json.dumps(ret_data), content_type = "application/json")

response={}
def searcherku(request):

	if request.method == 'POST':
		form = Searcher(request.POST or None)
		if form.is_valid():
			global url_data
			url_data = 'https://www.googleapis.com/books/v1/volumes?q=' + request.POST['search']
			return redirect('/')
			
			
def login(request):
    # context = RequestContext(request, {
    #     'request': request, 'user': request.user})
    # return render_to_response('login.html', context_instance=context)
    return render(request, 'login.html')


def logout(request):
	request.session.flush()
	auth_logout(request)
	return render(request, 'logout.html',response)
