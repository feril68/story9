from django.contrib import admin
from django.urls import path, include

from . import views


urlpatterns = [
	path('', views.daftar),
	path('api/', views.data_bukuku, name="data_bukuku"),
	path('post', views.searcherku)
        
]