from django import forms

class Searcher(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    search = forms.CharField(label='Search Other Books', required=True, max_length=100, widget=forms.TextInput(attrs=attrs))
